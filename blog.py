import os
import logging
from urllib.parse import urlparse
from datetime import datetime
import gitlab
from jinja2 import Environment, FileSystemLoader, select_autoescape
from slugify import slugify
import youtube_dl
from youtube_dl.utils import ExtractorError, DownloadError
from typing import Optional

# detection type hébergeur
VIDEO_HOSTS = [
    'youtu.be', 'www.youtube.com', 'm.youtube.com'
]

# GitLab API
gl = gitlab.Gitlab('https://gitlab.com', private_token=os.environ['GITLAB_TOKEN'])
project = gl.projects.get(os.environ['GITLAB_PROJECT'])

# Jinja
env = Environment(
    loader=FileSystemLoader('templates'),
    autoescape=select_autoescape(['html'])
)

# Logging
logging.basicConfig(format='%(levelname)s - %(message)s', level= getattr(logging, os.environ.get('LOG_LEVEL', 'INFO')))

# Issues
# closed = archivé, as_list = générateur paginé
issues = project.issues.list(state='opened', as_list=False)

class Blog:
    """Conversion d'une issue GitLab en blog"""

    iid: int  # id interne au projet, id du blog
    title: str  # title
    slug: str  # id + title slugifié
    source: str  # url dans la description
    link: str  # fichier html pour le blog
    posted: datetime  # date de création de l'issue = date de publication
    created: Optional[datetime]  # due_date de l'issue = date de l'œuvre
    filename: str  # fichié youtube-dl-é
    thumbnail: Optional[str]  # pochette / capture 

    def __init__(self, issue):
        self.iid = issue.iid
        self.title = issue.title
        self.slug = slugify(issue.title)
        self.source = issue.description
        self.link = '{s.iid}_{s.slug}.html'.format(s=self)
        self.posted = datetime.strptime(issue.created_at, "%Y-%m-%dT%H:%M:%S.%fZ")
        if issue.due_date:
            self.created = datetime.strptime(issue.due_date, "%Y-%m-%d")
    
    def __str__(self):
        return self.title
    
    @property
    def media(self) -> str:
        """vidéo ou audio ?"""
        if urlparse(self.source).hostname in VIDEO_HOSTS:
            return "video"
        return "audio"

    @property
    def mimetype(self) -> str:
        """media/codec pour tag audio ou video"""
        return "{media}/{ext}".format(media=self.media, ext=os.path.splitext(self.filename)[1][1:])

    @property
    def outputtpl(self) -> str:
        """format de nommage pour youtube-dl"""
        return "media/{s.iid}_{s.slug}.%(ext)s".format(s=self)


tpl_blog = env.get_template('blog.html')
tpl_index = env.get_template('index.html')

blogs = []

for issue in issues:
    blog = Blog(issue)
    logging.info('téléchargement %s' % blog)
    ydl_opts = {
        "download_archive": "media/archive.txt",
        "outtmpl": blog.outputtpl,
        "quiet": logging.getLogger().level != logging.DEBUG,
    }
    with youtube_dl.YoutubeDL(ydl_opts) as ytdl:
        try:
            result = ytdl.extract_info(blog.source)
        except (DownloadError, ExtractorError) as error:
            issue.labels = issue.labels + ['offline']
            issue.save()
            issue.notes.create({"body": "erreur `{}`".format(error)})
            continue
        blog.filename = ytdl.prepare_filename(result)
        blog.thumbnail = result.get("thumbnail")
        ytdl.download([blog.source])
    blogs.append(blog)

# page dédiées
for blog in blogs:
    logging.info('génération %s' % blog.link)
    tpl_blog.stream(blog=blog).dump('public/' + blog.link)

# index
logging.info('génération index')
tpl_index.stream(blogs=blogs).dump('public/index.html')


